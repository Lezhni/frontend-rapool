<?php

require_once($CFG['path']['classes'] . 'image.class.php');
require_once($CFG['path']['classes'] . 'gallery.class.php');

$obj['image'] = new image();
$obj['gallery'] = new gallery($obj);


$galleries = $obj['gallery']->get_galleries_list();
if (isset($galleries[0]['id']) )
	$galid = ($obj['url']->params['galid']) ? $obj['url']->params['galid'] : $galleries[0]['id'];
else
	$galid = 0;




switch($obj['url']->params['mode']) {

	case 'add':
		$item = $obj['gallery']->get_photo($obj['gallery']->add_photo($galid));
		$obj['smarty']->assign('photo', checkImage($item, 'gallery'));
		$obj['smarty']->assign('file_inc', 'admin/gallery/edit.tpl');
	break;

	case 'activate':
		$s = $obj['gallery']->get_photo($obj['url']->params['id']);
		$obj['tree']->set_content($s['id'], array('active' => ($s['active']) ? 0 : 1));
		display_manager();
	break;

	case 'delete':
		$s = $obj['gallery']->del_photo($obj['url']->params['id']);
		display_manager();
	break;

	case 'save':
		if($_FILES['image'] && eregi('jpeg', ($_FILES['image']['type']))) {
			move_uploaded_file($_FILES['image']['tmp_name'], 'images/data/gallery_' . $obj['url']->params['id'] . '.jpg');
		}
		else if($_FILES['image'] && eregi('gif', ($_FILES['image']['type']))) {
			move_uploaded_file($_FILES['image']['tmp_name'], 'images/data/gallery_' . $obj['url']->params['id'] . '.gif');
		}
		else if($_FILES['image'] && eregi('png', ($_FILES['image']['type']))) {
			move_uploaded_file($_FILES['image']['tmp_name'], 'images/data/gallery_' . $obj['url']->params['id'] . '.png');
		}
		if($_POST['delimg']) 
		{
			@unlink('images/data/gallery_' . $obj['url']->params['id'] . '.jpg');
			@unlink('images/data/gallery_' . $obj['url']->params['id'] . '.gif');
			@unlink('images/data/gallery_' . $obj['url']->params['id'] . '.png');
		}

		$obj['gallery']->set_content($obj['url']->params['id'], $_POST['photo']);
		//$obj['image']->image2jpeg($_FILES['image']['tmp_name'], 'gallery_' . $obj['url']->params['id']);
		//if($_POST['delimg']) @unlink('images/data/gallery_' . $obj['url']->params['id'] . '.jpg');
		display_manager();
	break;

	case 'edit':
		$item = $obj['gallery']->get_photo($obj['url']->params['id']);
		$obj['smarty']->assign('photo', checkImage($item, 'gallery'));
		$obj['smarty']->assign('file_inc', 'admin/gallery/edit.tpl');
	break;
	
	case 'up_item' :
		$obj['tree']->move_up($VAR['id']);
		header('Location: index.catid-' . $VAR['catid'] . '.page-' . $VAR['page'] . '.sid-' . $VAR['sid'] . '.html');
	break;

	case 'down_item' :
		$obj['tree']->move_down($VAR['id']);
		header('Location: index.catid-' . $VAR['catid'] . '.page-' . $VAR['page'] . '.sid-' . $VAR['sid'] . '.html');
	break;


	default:
		display_manager();
	break;


}

$obj['smarty']->assign('page', $obj['url']->params['page']);
$obj['smarty']->assign('galid', $galid);

function display_manager() {
	global $obj, $galid, $galleries;
	$obj['smarty']->assign('galleries', $galleries);
	$photos = $obj['gallery']->get_photos_list($galid);
	$obj['smarty']->assign('photos', checkImages($photos, 'gallery'));
	$obj['smarty']->assign('count', count($photos));
	$obj['smarty']->assign('file_inc', 'admin/gallery/main.tpl');
}


?>