$(document).ready(function() {
	$('.ce-d').dotdotdot();

	w = 0;
	$('#pagination li').each(function() {
		w = w + $(this).width() + 10;
	});
	$('#pagination').css('width', w+'px');

	$('#crw-rw').magnificPopup({
		type: 'inline',
		preloader: true,
	});

	$('.vi-m, .vi-b .title').click(function() {
		var attr = $(this).parent().find('.vi-fd').attr('style');
		if (typeof attr !== 'undefined' && attr !== false && $(this).parent().find('.vi-fd').css('display') == 'block') {
			$(this).parent().find('.vi-m').css('background','url(../img/btd.png) no-repeat top center');
			$(this).parent().find('.vi-fd').slideUp('slow');
		} else if (typeof attr !== 'undefined' && attr !== false && $(this).parent().find('.vi-fd').css('display') == 'none') {
			$(this).parent().find('.vi-m').css('background','url(../img/btu.png) no-repeat top center');
			$(this).parent().find('.vi-fd').slideDown('slow');
		} else {
			$(this).parent().find('.vi-m').css('background','url(../img/btu.png) no-repeat top center');
			$(this).parent().find('.vi-fd').slideDown('slow');
		}
		return false;
	});

	$(window).scroll(function() {
		if($(this).scrollTop() > 400) {
			if(!$('.man').hasClass('hide')) {
				$('.man').animate({
					right: '0px'
				}, 700).delay(3500).animate({
					right: '-89px'
				}, 700).addClass('hide');
			}
		}
		if($(this).scrollTop() > 200) {
			if(!$('.man2').hasClass('hide')) {
				$('.man2').animate({
					left: '0px'
				}, 700).delay(3500).animate({
					left: '-95px'
				}, 700).addClass('hide');
			}
		}
	});
});